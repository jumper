/* This file is part of Jumper
   Copyright (C) 1999-2005,2008,2013,2017,2020 Sergey Poznyakoff

   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jumper.h"

void
cons_raw_packet(u_char *user, const struct pcap_pkthdr *h, const u_char *p)
{
	u_int length = h->len;

	ip_handler((struct ip *)p, length);
}
