/* This file is part of Jumper
   Copyright (C) 2013, 2017, 2020 Sergey Poznyakoff
  
   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "jumper.h"
#include <grecs.h>

char *conffile = DEFAULT_CONFFILE;

int preprocess_only;

static int opt_debug_level = 0;
static int opt_foreground = 0;
static char *opt_pidfile = NULL;
static int opt_facility = -1;
static int lint_only = 0;

#define NITEMS(a) (sizeof(a)/sizeof((a)[0]))

int
sigv_set_action(int sigc, int *sigv, struct sigaction *sa)
{
	int i;
	
	for (i = 0; i < sigc; i++) {
		if (sigaction(sigv[i], &sa[i], NULL))
			return i+1;
	}
	return 0;
}

int
sigv_set_all(void (*handler)(int), int sigc, int *sigv,
	     struct sigaction *retsa)
{
	int i;
	struct sigaction sa;
	
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	
	for (i = 0; i < sigc; i++) {
		sa.sa_handler = handler;
		
		if (sigaction(sigv[i], &sa, retsa ? &retsa[i] : NULL)) {
			if (retsa) {
				int ec = errno;
				sigv_set_action(i, sigv, retsa);
				errno = ec;
			}
			return -1;
		}
	}
	return 0;
}

void
signal_setup(void (*sf) (int))
{
	static int sigv[] = { SIGTERM, SIGQUIT, SIGINT, SIGHUP, SIGALRM,
			      SIGUSR1, SIGUSR1, SIGCHLD };
	sigv_set_all(sf, NITEMS(sigv), sigv, NULL);
}

void
storepid(const char *pidfile)
{
	FILE *fp = fopen(pidfile, "w");
	if (!fp) {
		diag(LOG_ERR, "cannot open pidfile %s for writing: %s",
		     pidfile, strerror(errno));
	} else {
		fprintf(fp, "%lu\n", (unsigned long) getpid());
		fclose(fp);
	}
}

int signo = 0;

enum {
	command_none,
	command_reload,
	command_stop
};
	
int command = command_none;
int child_cleanup = 0;

void
sigmain(int sig)
{
	signo = sig;
	switch (signo) {
	case SIGCHLD:
		child_cleanup = 1;
		break;
	case SIGALRM:
		break;
	case SIGHUP:
		command = command_reload;
		break;
	default:
		command = command_stop;
	}
}

static fd_set fdset;
static int maxfd = -1;

static void
reload()
{
	struct grecs_node *tree;

	diag(LOG_INFO, "reloading configuration");
	config_preparse();
	tree = grecs_parse(conffile);
	if (!tree)
		return;
	if (config_finish(tree, 1))
		return;

	progman_decommission();
	listeners_decommission();

	listener_list_run_action(&clist, event_startup);
	listener_list_concat(&llist, &clist);

	listeners_init();
	switch (interfaces_init()) {
	case 0:
		break;
	case EX_CONFIG:
		break;
	default:
		listeners_cleanup();
		if (!llist.head)
			diag(LOG_CRIT, "nothing to listen");
	}
	
	FD_ZERO(&fdset);
	maxfd = -1;
	interfaces_fdset(&fdset, &maxfd);
	debug(1, ("configuration reload successful"));
}

#include "cmdline.h"

int
main(int argc, char **argv)
{
	int i;
	struct grecs_node *tree;
	
	set_program_name(argv[0]);
	config_init();
	parse_options(argc, argv, &i);

	argc -= i;
	argv += i;

	switch (argc) {
	default:
		diag(LOG_CRIT, "too many arguments");
		exit(EX_USAGE);
	case 1:
		conffile = argv[0];
		break;
	case 0:
		break;
	}

        if (preprocess_only)
                exit(grecs_preproc_run(conffile, grecs_preprocessor) ?
                     EX_CONFIG : 0);
	
	tree = grecs_parse(conffile);
	if (!tree)
		exit(EX_CONFIG);

	if (config_finish(tree, 0))
		exit(EX_CONFIG);

	if (opt_debug_level)
		config.debug_level += opt_debug_level;
	if (opt_foreground)
		config.foreground = opt_foreground;
	if (opt_pidfile)
		config.pidfile = opt_pidfile;
	if (opt_facility != -1)
		config.facility = opt_facility;
	if (!config.foreground && config.facility <= 0)
		config.facility = LOG_DAEMON;

	listener_list_concat(&llist, &clist);
	listeners_init();
	switch (interfaces_init()) {
	case 0:
		break;
	case EX_CONFIG:
		exit(EX_CONFIG);
	default:
		listeners_cleanup();
		if (!llist.head) {
			diag(LOG_CRIT, "nothing to listen");
			exit(EX_UNAVAILABLE);
		}
	}

	FD_ZERO(&fdset);
	maxfd = -1;
	interfaces_fdset(&fdset, &maxfd);

	if (lint_only)
		return 0;

	if (config.facility > 0) {
		openlog(config.tag, LOG_PID, config.facility);
		grecs_log_to_stderr = 0;
	}

	if (!config.foreground) {
		if (daemon(0, 0) < 0) {
			diag(LOG_CRIT, "daemon: %s", strerror(errno));
			exit(EX_OSERR);
		}
		log_to_stderr = -1;
	}

	diag(LOG_INFO, "%s %s started", program_name, VERSION);
	storepid(config.pidfile);

	signal_setup(sigmain);
	listeners_run_action(event_startup);

	while (command != command_stop) {
		int rc;
		struct timeval tv;
		fd_set rdset;

		if (child_cleanup) {
			progman_cleanup();
			child_cleanup = 0;
		}
		if (command == command_reload) {
			reload();
			command = command_none;
		}
		
		rdset = fdset;
		rc = select(maxfd + 1, &rdset, NULL, NULL,
			    progman_timeout(&tv));
		if (rc < 0) {
			if (errno == EINTR || errno == EAGAIN)
				continue;
			diag(LOG_ERR, "select: %s", strerror(errno));
			exit(EX_OSERR);
		} else if (rc == 0) {
			progman_expire();
			continue;
		}

		interfaces_poll(&rdset);
	}
	
	progman_terminate();
	listeners_run_action(event_cleanup);
	
	diag(LOG_INFO, "%s %s stopped", program_name, VERSION);

	unlink(config.pidfile);
	
	return 0;
}
