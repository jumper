/* This file is part of Jumper
   Copyright (C) 2013, 2017, 2020 Sergey Poznyakoff
  
   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jumper.h"
#include <sys/wait.h>


const char *
ipv4_to_string(uint32_t ip)
{
	static char buf[16];
	
	sprintf(buf, "%u.%u.%u.%u",
		(ip >> 24) & 0xff,
		(ip >> 16) & 0xff,
		(ip >> 8) & 0xff,
		ip & 0xff);

	return buf;
}

int
ipv4_match_p(ipv4_match_list_t *mp, uint32_t ip)
{
	for (; mp; mp = mp->next)
		if (mp->cidr.addr == (mp->cidr.netmask & ip))
			return 1;
	return 0;
}

int
ipv4_match_list_cmp(ipv4_match_list_t *a, ipv4_match_list_t *b)
{
	 for (; a && b && a->cidr.addr == b->cidr.addr &&
		     a->cidr.netmask == b->cidr.netmask;
	     a = a->next, b = b ->next);
	if (!a && !b)
		return 0;
	return 1;
}

void
ipv4_match_list_free(ipv4_match_list_t *a)
{
	while (a) {
		ipv4_match_list_t *next = a->next;
		free(a);
		a = next;
	}
}

static int
event_cmp(event_t *a, event_t *b)
{
	for (; a && b; a = a->next, b = b ->next) {
		if (a->type != b->type)
			return 1;
		switch (a->type) {
		case event_startup:
		case event_cleanup:
			continue;
		default:
			if (a->code != b->code)
				return 1;
		}
	}
	if (!a && !b)
		return 0;
	return 1;	
}

static void
event_free(event_t *a)
{
	while (a) {
		event_t *next = a->next;
		free(a);
		a = next;
	}
}

static int
act_cmp(action_t *a, action_t *b)
{
	for (; a && b; a = a->next, b = b ->next) {
		if (event_cmp(a->evt, b->evt))
			return 1;
		if (strcmp(a->command, b->command))
			return 1;
		if (envcmp(a->env, b->env))
			return 1;
		if (a->options != b->options)
			return 1;
		if (a->timeout != b->timeout)
			return 1;
	}
	if (!a && !b)
		return 0;
	return 1;
}

static void
act_free(action_t *a)
{
	while (a) {
		action_t *next = a->next;
		event_free(a->evt);
		free(a->command);
		envfree(a->env);
		a = next;
	}
}

void
listener_list_add(listener_list_t *lst, listener_t *lp)
{
	lp->prev = lst->tail;
	if (lst->tail)
		lst->tail->next = lp;
	else
		lst->head = lp;
	lst->tail = lp;
}

void
listener_list_remove(listener_list_t *lst, listener_t *lp)
{
	listener_t *p;

	p = lp->prev;
	if (p)
		p->next = lp->next;
	else
		lst->head = lp->next;

	p = lp->next;
	if (p)
		p->prev = lp->prev;
	else
		lst->tail = lp->prev;
}

void
listener_list_concat(listener_list_t *a, listener_list_t *b)
{
	if (!b->head)
		return;
	b->head->prev = a->tail;
	if (a->tail)
		a->tail->next = b->head;
	else
		a->head = b->head;
	a->tail = b->tail;
	b->head = b->tail = NULL;
}

void
listener_list_free(listener_list_t *lst)
{
	listener_t *p;

	for (p = lst->head; p; ) {
		listener_t *next = p->next;
		listener_free(p);
		p = next;
	}
	lst->head = lst->tail = NULL;
}			

listener_t *
listener_list_find_id(listener_list_t *lst, const char *id)
{
	listener_t *p;

	for (p = lst->head; p; p = p->next)
		if (strcmp(p->id, id) == 0)
			break;
	return p;
}


listener_list_t llist;
listener_list_t clist;

void
listener_fixup(listener_t *lp)
{
	lp->locus.beg.file = estrdup(lp->locus.beg.file);
	lp->locus.end.file = estrdup(lp->locus.end.file);
}

void
listener_decommission(listener_t *lp)
{
	lp->decommission = 1;
	interface_unref(lp->iface);
}

void
listener_free(listener_t *lp)
{
	free(lp->id);
	ipv4_match_list_free(lp->match_source);
	ipv4_match_list_free(lp->match_dest);
	free(lp->prog);
	envfree(lp->env);
	act_free(lp->act_head);
	free(lp->locus.beg.file);
	free(lp->locus.end.file);
}
	

/* Compare two listeners.  Return 0 if equal. */
int
listener_cmp(listener_t *a, listener_t *b)
{
	if (a->iface != b->iface) {
		debug(5, ("%s/%s: interfaces differ", a->id, b->id));
		return 1;
	}

	if (strcmp(a->prog, b->prog)) {
		debug(5, ("%s/%s: commands differ", a->id, b->id));
		return 1;
	}
	
	if (ipv4_match_list_cmp(a->match_source, b->match_source)) {
		debug(5, ("%s/%s: match-source lists differ", a->id, b->id));
		return 1;
	}

	if (ipv4_match_list_cmp(a->match_dest, b->match_dest)) {
		debug(5, ("%s/%s: match-destination lists differ", a->id, b->id));
		return 1;
	}

	if (envcmp(a->env, b->env)) {
		debug(5, ("%s/%s: environments differ", a->id, b->id));
		return 1;
	}

	if (a->options != b->options) {
		debug(5, ("%s/%s: options differ", a->id, b->id));
		return 1;
	}

	if (act_cmp(a->act_head, b->act_head)) {
		debug(5, ("%s/%s: action lists differ", a->id, b->id));
		return 1;
	}
		
	return 0;
}

void
listeners_init()
{
	listener_t *lp;
	
	for (lp = llist.head; lp; lp = lp->next)
		interface_build_expr(lp->iface, lp->match_source,
				     lp->match_dest);
}

void
listeners_cleanup()
{
	listener_t *lp;
	
	for (lp = llist.head; lp; ) {
		listener_t *next = lp->next;
		if (!interface_ok(lp->iface)) {
			interface_unref(lp->iface);
			listener_list_remove(&llist, lp);
			listener_free(lp);
		}
		lp = next;
	}
}
	


void
listener_kve_init(char **kve, int kvn, listener_t *lp, ...)
{
	int i = 0;
	static char linebuf[64];
	va_list ap;
	char *kw, *val;
	
	if (kvn < KVE_MINSIZE) {
		diag(LOG_CRIT, "%s:%d: INTERNAL ERROR", __FILE__, __LINE__);
		abort();
	}
	
	kve[i++] = "program";
	kve[i++] = (char*)program_name;

	kve[i++] = "file";
	kve[i++] = lp->locus.beg.file;

	kve[i++] = "line";
	snprintf(linebuf, sizeof(linebuf), "%u", lp->locus.beg.line);
	kve[i++] = linebuf;

	va_start(ap, lp);
	while ((kw = va_arg(ap,char*)) && (val = va_arg(ap,char*))) {
		if (i + 2 >= kvn) {
			diag(LOG_CRIT, "%s:%d: INTERNAL ERROR",
			     __FILE__, __LINE__);
			abort();
		}
		kve[i++] = kw;
		kve[i++] = val;
	}
	va_end(ap);
	kve[i] = NULL;
}

static char *event_str[] = {
	[event_startup]   = "startup",
	[event_cleanup]   = "cleanup",
	[event_exit]      = "exit",
	[event_signal]    = "signal",
	[event_heartbeat] = "heartbeat",
};	

static event_t *
event_match_type(action_t *act, enum event_type type)
{
	event_t *evt;

	for (evt = act->evt; evt; evt = evt->next)
		if (evt->type == type)
			break;
	return evt;
}

static event_t *
event_match(action_t *act, enum event_type type, int code)
{
	event_t *evt;

	for (evt = act->evt; evt; evt = evt->next)
		if (evt->type == type && evt->code == code)
			break;
	return evt;
}

static int runaction(listener_t *lp, action_t *act, event_t *evt);

int
listener_run_action(listener_t *lp, int a)
{
	action_t *act;
	int err = 0;
	
	for (act = lp->act_head; act; act = act->next) {
		event_t *evt = event_match_type(act, a);
		if (evt && runaction(lp, act, evt))
			err++;
	}
	return err;
}

int
listener_list_run_action(listener_list_t *lst, int a)
{
	listener_t *lp;
	int err = 0;
	
	for (lp = lst->head; lp; lp = lp->next)
		err += listener_run_action(lp, a);
	return err;
}

int
listeners_run_action(int a)
{
	return listener_list_run_action(&llist, a);
}

listener_t *
listener_locate(uint32_t src, uint32_t dst)
{
	listener_t *lp;

	for (lp = llist.head; lp; lp = lp->next) {
		if ((!lp->match_source ||
		     ipv4_match_p(lp->match_source, src)) &&
		    ipv4_match_p(lp->match_dest, dst))
			break;
	}
	return lp;
}

listener_t *
listener_lookup_pid(pid_t pid)
{
	listener_t *lp;
	for (lp = llist.head; lp; lp = lp->next)
		if (lp->pid == pid)
			break;
	return lp;
}

void
listeners_decommission()
{
	listener_t *lp;

	for (lp = llist.head; lp; ) {
		listener_t *next = lp->next;
		if (lp->decommission) {
			listener_list_remove(&llist, lp);
			listener_free(lp);
		}
		lp = next;
	}
}

void
listeners_undo_decommission()
{
	listener_t *lp;

	for (lp = llist.head; lp; lp = lp->next)
		lp->decommission = 0;
}	

void
listener_print_status(listener_t *lp)
{
	if (WIFEXITED(lp->progstat)) {
		if (WEXITSTATUS(lp->progstat) == 0)
			debug(1, ("%s:%d: %s (%lu) exited successfully",
				  lp->locus.beg.file, lp->locus.beg.line,
				  lp->prog, (unsigned long) lp->pid));
		else
			diag(LOG_NOTICE,
			     "%s:%d: %s (%lu) exited with status %d",
			     lp->locus.beg.file, lp->locus.beg.line,
			     lp->prog, (unsigned long) lp->pid,
			     WEXITSTATUS(lp->progstat));
	} else if (WIFSIGNALED(lp->progstat))
		diag(LOG_NOTICE,
		     "%s:%d: %s (%lu) terminated on signal %d",
		     lp->locus.beg.file, lp->locus.beg.line,
		     lp->prog, (unsigned long) lp->pid,
		     WTERMSIG(lp->progstat));
	else if (WIFSTOPPED(lp->progstat))
		diag(LOG_NOTICE,
		     "%s:%d: %s (%lu) stopped",
		     lp->locus.beg.file, lp->locus.beg.line,
		     lp->prog, (unsigned long) lp->pid);

#ifdef WCOREDUMP
	if (WCOREDUMP(lp->progstat))
		diag(LOG_NOTICE,
		     "%s:%d: %s (%lu) dumped core",
		     lp->locus.beg.file, lp->locus.beg.line,
		     lp->prog, (unsigned long) lp->pid);
#endif

	lp->status = stat_down;
} 

static int
runaction(listener_t *lp, action_t *act, event_t *evt)
{
	char *kve[KVE_MINSIZE + 8];
	char codebuf[64];
	char statbuf[64];
	char pidbuf[80], *pid;
	
	snprintf(statbuf, sizeof(statbuf), "%u", lp->progstat);
	snprintf(codebuf, sizeof(codebuf), "%u", evt->code);
	if (lp->status == stat_up) {
		snprintf(pidbuf, sizeof(pidbuf), "%lu",
			 (unsigned long)lp->pid);
		pid = pidbuf;
	} else
		pid = NULL;
	listener_kve_init(kve, sizeof(kve)/sizeof(kve[0]),
			  lp,
			  "event", event_str[evt->type],
			  "status", statbuf,
			  "exitcode", codebuf,
			  "pid", pid,
			  NULL);

	if (progman_start(type_action, lp, &act->locus, act->options,
			  act->timeout, act->command, act->env, kve))
		return 1;

	lp->act_count++;
	return 0;
}

void
onexit_reaction(listener_t *lp)
{
	action_t *act;
	int code;
	enum event_type type;
	event_t *evt;
	
	lp->status = stat_onexit;
	if (WIFEXITED(lp->progstat)) {
		type = event_exit;
		code = WEXITSTATUS(lp->progstat);
	} else if (WIFSIGNALED(lp->progstat)) {
		type = event_signal;
		code = WTERMSIG(lp->progstat);
	}

	for (act = lp->act_head; act; act = act->next) {
		if ((evt = event_match(act, type, code)))
			runaction(lp, act, evt);
	}
	if (lp->act_count == 0)
		lp->status = stat_down;
}

void
listener_proc_report(listener_t *lp)
{
	listener_print_status(lp);
	if (lp->decommission)
		listener_run_action(lp, event_cleanup);
	else
		onexit_reaction(lp);
}

void
listener_start(listener_t *lp, char **kve)
{
	progman_start(type_listener, lp, &lp->locus, lp->options,
		      config.heartbeat, lp->prog, lp->env, kve);
}

