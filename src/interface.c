/* This file is part of Jumper
   Copyright (C) 2013, 2017, 2020 Sergey Poznyakoff
  
   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jumper.h"

int snaplen = 96;
int promisc_option = 1;

struct callback {
	pcap_handler    function;
	int             type;
};


static struct callback callbacks[] = {
	{ cons_ether_packet, DLT_EN10MB },
	{ cons_ether_packet, DLT_IEEE802 },
	{ cons_raw_packet, DLT_RAW },
	{ cons_ppp_packet, DLT_PPP },
	{ NULL, DLT_FDDI },
	{ NULL, DLT_SLIP },
	{ NULL, DLT_NULL },
	{ NULL, 0 },
};

static pcap_handler
lookup_pcap_callback(int type)
{
	struct callback *callback;

	for (callback = callbacks; callback->function; callback++)
		if (type == callback->type)
			return callback->function;
	return NULL;
}


struct interface {
	struct interface *prev, *next;
	int refcnt;              /* Number of listeners using this interface */
	char *name;              /* Interface name */
	pcap_t *pcap;            /* Pcap handler */
	pcap_handler hfun;       /* Packet handler function */ 
	struct grecs_txtacc *acc;/* Expression accumulator */
	char *expr;              /* Compiled expression */  
};

static struct interface *if_head, *if_tail;

struct interface *
interface_lookup(const char *name)
{
	struct interface *ip;
	
	for (ip = if_head; ip; ip = ip->next) {
		if (strcmp(ip->name, name) == 0)
			return ip;
	}

	ip = emalloc(sizeof(*ip));
	ip->next = NULL;
	ip->prev = if_tail;
	ip->pcap = NULL;
	ip->name = estrdup(name);
	ip->acc  = NULL;
	ip->expr = NULL;
	
	if (if_tail)
		if_tail->next = ip;
	else
		if_head = ip;
	if_tail = ip;
	return ip;
}

void
interface_deinit(struct interface *ifp)
{
	grecs_txtacc_free(ifp->acc);
	ifp->acc = NULL;
	ifp->expr = NULL;
}

void
interface_ref(struct interface *ifp)
{
	++ifp->refcnt;
	if (ifp->expr)
		interface_deinit(ifp);
}

void
interface_unref(struct interface *ifp)
{
	if (--ifp->refcnt == 0) {
		struct interface *p;

		p = ifp->prev;
		if (p)
			p->next = ifp->next;
		else
			if_head = ifp->next;

		p = ifp->next;
		if (p)
			p->prev = ifp->prev;
		else
			if_tail = ifp->prev;

		if (ifp->pcap)
			pcap_close(ifp->pcap);
		if (ifp->acc)
			grecs_txtacc_free(ifp->acc);
		free(ifp->name);
		free(ifp);
	} else if (ifp->expr)
		interface_deinit(ifp);
}

int
interface_ok(struct interface *iface)
{
	return iface && iface->pcap;
}

int
interfaces_init()
{
	struct interface *iface;
	pcap_t *pd;
	char errbuf[PCAP_ERRBUF_SIZE];
	struct bpf_program bpfcode;
	int ret = 0;
	
	if (!if_head) {
		diag(LOG_CRIT, "no listeners configured");
		return EX_CONFIG;
	}

	for (iface = if_head; iface; iface = iface->next) {
		if (!iface->expr) {
			grecs_txtacc_grow_char(iface->acc, 0);
			iface->expr = grecs_txtacc_finish(iface->acc, 0);
		}
		debug(2, ("iface %s, expr %s",
			  iface->name, iface->expr));
			
		pd = pcap_open_live(iface->name, snaplen,
				    !promisc_option,
				    1000, errbuf);
		if (!pd) {
			diag(LOG_ERR, "pcap_open_live: %s", errbuf);
			iface->pcap = NULL;
			ret = EX_OSERR;
			continue;
		}
		iface->pcap = pd;
		iface->hfun = lookup_pcap_callback(pcap_datalink(pd));
		
		if (!iface->hfun) {
			diag(LOG_ERR,
			     "interface %s: unknown data link type 0x%x",
			     iface->name, pcap_datalink(pd));
			pcap_close(iface->pcap);
			iface->pcap = NULL;
			ret = EX_SOFTWARE;
			continue;
		}
		
		if (pcap_compile(iface->pcap, &bpfcode, iface->expr,
				 1, 0) < 0) {
			diag(LOG_ERR, "pcap_compile(%s): %s",
			     iface->expr, pcap_geterr(iface->pcap));
			pcap_close(iface->pcap);
			iface->pcap = NULL;
			ret = EX_OSERR;
			continue;
		}

		if (pcap_setfilter(iface->pcap, &bpfcode) < 0) {
			diag(LOG_ERR, "pcap_setfilter: %s",
			     pcap_geterr(iface->pcap));
			pcap_close(iface->pcap);
			iface->pcap = NULL;
			ret = EX_OSERR;
		}
	}
	return ret;
}

void
interfaces_fdset(fd_set *fdset, int *fdmax)
{
	struct interface *ifp;
	
	for (ifp = if_head; ifp; ifp = ifp->next) {
		int fd = pcap_fileno(ifp->pcap);
		if (fd > *fdmax)
			*fdmax = fd;
		FD_SET(fd, fdset);
	}
}

void
interfaces_poll(fd_set *fdset)
{
	struct interface *ifp;
	
	for (ifp = if_head; ifp; ifp = ifp->next) {
		int fd = pcap_fileno(ifp->pcap);
		if (FD_ISSET(fd, fdset)) {
			struct pcap_pkthdr hdr;
			const u_char *data = pcap_next(ifp->pcap, &hdr);
			if (!data)
				continue;
			ifp->hfun(NULL, &hdr, data);
		}
	}
}

void
interface_build_expr(struct interface *iface, ipv4_match_list_t *match_source,
		     ipv4_match_list_t *match_dest)
{
	ipv4_match_list_t *mp;
	int delim;
	
	if (iface->acc)
		grecs_txtacc_grow_string(iface->acc, " or ");
	else
		iface->acc = grecs_txtacc_create();
	
	grecs_txtacc_grow_string(iface->acc, " ( ");
	delim = 0;
	if (match_source) {
		grecs_txtacc_grow_string(iface->acc, " ( ");
		for (mp = match_source; mp; mp = mp->next) {
			if (delim)
				grecs_txtacc_grow_string(iface->acc, " or");
			grecs_txtacc_grow_string(iface->acc, " src net ");
			grecs_txtacc_grow_string(iface->acc,
						 ipv4_to_string(mp->cidr.addr));
			grecs_txtacc_grow_string(iface->acc, " mask ");
			grecs_txtacc_grow_string(iface->acc,
						 ipv4_to_string(mp->cidr.netmask));
			delim = 1;
		}
		grecs_txtacc_grow_string(iface->acc, " ) and ");
	}
	delim = 0;
	grecs_txtacc_grow_string(iface->acc, " ( ");
	for (mp = match_dest; mp; mp = mp->next) {
		if (delim)
			grecs_txtacc_grow_string(iface->acc, " or");
		grecs_txtacc_grow_string(iface->acc, " dst net ");
		grecs_txtacc_grow_string(iface->acc,
					 ipv4_to_string(mp->cidr.addr));
		grecs_txtacc_grow_string(iface->acc, " mask ");
		grecs_txtacc_grow_string(iface->acc,
					 ipv4_to_string(mp->cidr.netmask));
		delim = 1;
	}
	grecs_txtacc_grow_string(iface->acc, " ) ");
	grecs_txtacc_grow_string(iface->acc, " ) ");
}
