/* This file is part of Jumper
   Copyright (C) 2013, 2017, 2020 Sergey Poznyakoff
  
   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "jumper.h"
#include <stddef.h>

struct jumper_config config, newcfg;

static struct transtab kwpri[] = {
	{ "emerg", LOG_EMERG },
	{ "alert", LOG_ALERT },
	{ "crit", LOG_CRIT },
	{ "err", LOG_ERR },
	{ "warning", LOG_WARNING },
	{ "notice", LOG_NOTICE },
	{ "info", LOG_INFO },
	{ "debug", LOG_DEBUG },
	{ NULL }
};

static struct transtab kwfac[] = {
	{ "user",    LOG_USER },
	{ "daemon",  LOG_DAEMON },
	{ "auth",    LOG_AUTH },
	{ "authpriv",LOG_AUTHPRIV },
	{ "mail",    LOG_MAIL },
	{ "cron",    LOG_CRON },
	{ "local0",  LOG_LOCAL0 },
	{ "local1",  LOG_LOCAL1 },
	{ "local2",  LOG_LOCAL2 },
	{ "local3",  LOG_LOCAL3 },
	{ "local4",  LOG_LOCAL4 },
	{ "local5",  LOG_LOCAL5 },
	{ "local6",  LOG_LOCAL6 },
	{ "local7",  LOG_LOCAL7 },
	{ NULL }
};

int
get_facility(const char *arg, int *retval)
{
	int f;
	char *p;

	errno = 0;
	f = strtoul(arg, &p, 0);
	if (!(*p == 0 && errno == 0) && trans_strtotok(kwfac, arg, &f)) 
		return -1;
	*retval = f;
	return 0;
}

int
get_priority(const char *arg, int *retval)
{
	int f;
	char *p;

	errno = 0;
	f = strtoul(arg, &p, 0);
	if (!(*p == 0 && errno == 0) && trans_strtotok(kwpri, arg, &f))
		return -1;
	*retval = f;
	return 0;
}

#define ASSERT_SCALAR(cmd, locus)					\
	if ((cmd) != grecs_callback_set_value) {			\
	        grecs_error(locus, 0, "Unexpected block statement");	\
		return 1;						\
	}

static char *
my_grecs_basic_type_string(int t)
{
	switch (t) {
	case GRECS_TYPE_STRING:
		return "string";
	case GRECS_TYPE_ARRAY:
		return "array";
	case GRECS_TYPE_LIST:
		return "list";
	}
	return "unknown";
}

int
assert_grecs_value_type(grecs_locus_t *locus,
			const grecs_value_t *value, int type)
{
	if (GRECS_VALUE_EMPTY_P(value)) {
		grecs_error(locus, 0, "expected %s",
			    my_grecs_basic_type_string(type));
		return 1;
	}
	if (value->type != type) {
		grecs_error(locus, 0, "expected %s, but found %s",
			    my_grecs_basic_type_string(type),
			    my_grecs_basic_type_string(value->type));
		return 1;
	}
	return 0;
}

static int
cb_syslog_facility(enum grecs_callback_command cmd, grecs_node_t *node,
		   void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;

	ASSERT_SCALAR(cmd, locus);
	if (assert_grecs_value_type(&value->locus, value, GRECS_TYPE_STRING))
		return 1;

	if (get_facility(value->v.string, varptr))
		grecs_error(&value->locus, 0,
			    "Unknown syslog facility `%s'",
			    value->v.string);
	return 0;
}

static struct grecs_keyword syslog_kw[] = {
	{ "facility",
	  "name",
	  "Set syslog facility. Arg is one of the following: user, daemon, "
	  "auth, authpriv, mail, cron, local0 through local7 "
	  "(case-insensitive), or a facility number.",
	  grecs_type_string, GRECS_DFLT,
	  &newcfg.facility, 0, cb_syslog_facility },
	{ "tag", "string", "Tag syslog messages with this string",
	  grecs_type_string, GRECS_DFLT,
	  &newcfg.tag },
	{ "print-priority", "arg",
	  "Prefix each message with its priority",
	  grecs_type_bool, GRECS_DFLT,
	  &newcfg.print_priority },
	{ NULL },
};

static int
cb_interface(enum grecs_callback_command cmd, grecs_node_t *node,
	     void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	struct interface **iface = varptr;
	
	ASSERT_SCALAR(cmd, locus);
	if (assert_grecs_value_type(&value->locus, value, GRECS_TYPE_STRING))
		return 1;
	*iface = interface_lookup(value->v.string);
	return 0;
}

int
str_to_cidr(grecs_value_t *val, ipv4_match_list_t **phead,
	    ipv4_match_list_t **ptail)
{
	ipv4_cidr_t cidr;
	ipv4_match_list_t *entry;
	char *p;
	struct in_addr addr;
	char *str = val->v.string;
	unsigned long n;
	uint32_t mask;
	
	p = strchr(str, '/');
	if (p) {
		char *q;
		
		*p++ = 0;

		n = strtoul(p, &q, 10);
		if (*q == 0) {
			if (n < 0 || n > 32) {
				grecs_error(&val->locus, 0,
					    "invalid network mask");
				return 1;
			}

			mask = 0xfffffffful >> (32-n);
			mask <<= (32-n);

			cidr.netmask = mask;
		} else {
			if (inet_aton(p, &addr) == 0) {
				grecs_error(&val->locus, 0,
					    "invalid network mask");
				return 1;
			}
			cidr.netmask = ntohl(addr.s_addr);
		}
	} else
		cidr.netmask = 0xfffffffful;

	if (inet_aton(str, &addr) == 0) {
		grecs_error(&val->locus, 0, "invalid network address");
		return 1;
	}
	cidr.addr = ntohl(addr.s_addr);

	entry = emalloc(sizeof(*entry));
	entry->next = NULL;
	entry->cidr = cidr;

	if (*ptail)
		(*ptail)->next = entry;
	else
		*phead = entry;
	*ptail = entry;
	
	return 0;
}

static int
cb_match_list(enum grecs_callback_command cmd, grecs_node_t *node,
	      void *varptr, void *cb_data)
{
	struct grecs_list_entry *ep;
	grecs_value_t *value = node->v.value;
	ipv4_match_list_t **listptr = varptr;
	ipv4_match_list_t *head = NULL, *tail = NULL;
	
	ASSERT_SCALAR(cmd, &node->locus);
	switch (value->type) {
	case GRECS_TYPE_STRING:
		if (str_to_cidr(value, &head, &tail))
			return 1;
		break;

	case GRECS_TYPE_LIST:
		for (ep = value->v.list->head; ep; ep = ep->next) {
			grecs_value_t *vp = ep->data;
			if (assert_grecs_value_type(&vp->locus, vp,
						    GRECS_TYPE_STRING))
				return 1;
			if (str_to_cidr(vp, &head, &tail)) {
				ipv4_match_list_free(head);
				return 1;
			}
		}
		break;

	default:
		grecs_error(&value->locus, 0,
			    "expected CIDR or list of CIDRs");
		return 1;
	}
	
	*listptr = head;

	return 0;
}

static void
value_to_options(grecs_value_t *val, int *options)
{
#define NEGATE 0x8000
	static struct transtab opttab[] = {
		{ "nowait", OPT_NOWAIT },
		{ "wait",   NEGATE | OPT_NOWAIT },
		{ "stdout", OPT_STDOUT },
		{ "stderr", OPT_STDERR },
		{ "nullin", OPT_NULLIN },
		{ "shell",  OPT_SHELL },
		{ NULL }
	};
	int t;
	
	if (trans_strtotok(opttab, val->v.string, &t)) {
		grecs_error(&val->locus, 0, "unrecognized option");
		return;
	}
	if (t & NEGATE)
		*options &= ~(t & ~NEGATE);
	else
		*options |= t;
}

static int
cb_option(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
        grecs_locus_t *locus = &node->locus;
	int *options = varptr;
	grecs_value_t *val = node->v.value;
	struct grecs_list_entry *ep;
	
	ASSERT_SCALAR(cmd, locus);
	switch (val->type) {
	case GRECS_TYPE_STRING:
		value_to_options(val, options);
		break;

	case GRECS_TYPE_LIST:
		for (ep = val->v.list->head; ep; ep = ep->next)	{
			grecs_value_t *vp = ep->data;
			if (assert_grecs_value_type(&vp->locus, vp,
						    GRECS_TYPE_STRING))
				return 1;
			value_to_options(vp, options);
		}
		break;

	default:
		grecs_error(&val->locus, 0, "bad number of arguments");
	}
	return 0;
}

static int
parse_environ(grecs_value_t *val, int off, char ***penv)
{
	struct grecs_list_entry *ep;
	int i;
	
	switch (val->type) {
	case GRECS_TYPE_STRING:
		if (assert_grecs_value_type(&val->locus, val,
					    GRECS_TYPE_STRING))
			return 1;
		*penv = ecalloc(2, sizeof((*penv)[0]));
		(*penv)[0] = estrdup(val->v.string);
		(*penv)[1] = NULL;
		break;
		
	case GRECS_TYPE_ARRAY:
		*penv = ecalloc(val->v.arg.c - off + 1, sizeof((*penv)[0]));
		for (i = off; i < val->v.arg.c; i++) {
			if (assert_grecs_value_type(&val->v.arg.v[i]->locus,
						    val->v.arg.v[i],
						    GRECS_TYPE_STRING))
				return 1;
			(*penv)[i] = estrdup(val->v.arg.v[i]->v.string);
		}
		(*penv)[i] = NULL;
		break;

	case GRECS_TYPE_LIST:
		*penv = ecalloc(val->v.list->count + 1, sizeof((*penv)[0]));
		for (i = 0, ep = val->v.list->head; ep; ep = ep->next, i++) {
			grecs_value_t *vp = ep->data;
			if (assert_grecs_value_type(&vp->locus, vp,
						    GRECS_TYPE_STRING))
				return 1;
			(*penv)[i] = estrdup(vp->v.string);
		}
		(*penv)[i] = NULL;
	}
	return 0;
}	

static int
cb_environ(enum grecs_callback_command cmd, grecs_node_t *node,
	   void *varptr, void *cb_data)
{
        grecs_locus_t *locus = &node->locus;
	grecs_value_t *val = node->v.value;
	char ***penv = varptr;
	
	ASSERT_SCALAR(cmd, locus);
	return parse_environ(val, 0, penv);
}		

#define S(s) { #s, s }
static struct transtab ex_trans[] = {
	S(EX_OK),
	S(EX_USAGE),
	S(EX_DATAERR),
	S(EX_NOINPUT),
	S(EX_NOUSER),
	S(EX_NOHOST),
	S(EX_UNAVAILABLE),
	S(EX_SOFTWARE),
	S(EX_OSERR),
	S(EX_OSFILE),
	S(EX_CANTCREAT),
	S(EX_IOERR),
	S(EX_TEMPFAIL),
	S(EX_PROTOCOL),
	S(EX_NOPERM),
	S(EX_CONFIG),
	{NULL}
};

static struct transtab sig_trans[] = {
	S(SIGHUP),
	S(SIGINT),
	S(SIGQUIT),
	S(SIGILL),
	S(SIGTRAP),
	S(SIGABRT),
	S(SIGIOT),
	S(SIGBUS),
	S(SIGFPE),
	S(SIGKILL),
	S(SIGUSR1),
	S(SIGSEGV),
	S(SIGUSR2),
	S(SIGPIPE),
	S(SIGALRM),
	S(SIGTERM),
#ifdef SIGSTKFLT
	S(SIGSTKFLT),
#endif
	S(SIGCHLD),
	S(SIGCONT),
	S(SIGSTOP),
	S(SIGTSTP),
	S(SIGTTIN),
	S(SIGTTOU),
#ifdef SIGURG
	S(SIGURG),
#endif
#ifdef SIGXCPU
	S(SIGXCPU),
#endif
#ifdef SIGXFSZ
	S(SIGXFSZ),
#endif
#ifdef SIGVTALRM
	S(SIGVTALRM),
#endif
#ifdef SIGPROF
	S(SIGPROF),
#endif
#ifdef SIGWINCH
	S(SIGWINCH),
#endif
#ifdef SIGPOLL
	S(SIGPOLL),
#endif
#ifdef SIGIO
	S(SIGIO),
#endif
#ifdef SIGPWR
	S(SIGPWR),
#endif
#ifdef SIGSYS
	S(SIGSYS),
#endif
	{NULL}
};
 
#undef S

static event_t *
parse_event(grecs_value_t *val)
{
	char *arg = val->v.string;
	int code;
	enum event_type type;
	event_t *evt;
	char *p;
	unsigned long n;
	
	if (trans_strtotok(ex_trans, arg, &code) == 0) {
		type = event_exit;
	} else if (trans_strtotok(sig_trans, arg, &code) == 0) {
		type = event_signal;
	} else if (strncmp(arg, "EX+", 3) == 0) {
		code = strtoul(arg+3, &p, 10);
		if (*p) {
			grecs_error(&val->locus, 0, "invalid exit code");
			return NULL;
		}
		type = event_exit;
	} else if (strncmp(arg, "SIG+", 4) == 0) {
		code = strtoul(arg+4, &p, 10);
		if (*p) {
			grecs_error(&val->locus, 0, "invalid signal number");
			return NULL;
		}
		type = event_signal;
	} else if (strcmp(arg, "STARTUP") == 0) {
		type = event_startup;
	} else if (strcmp(arg, "CLEANUP") == 0) {
		type = event_cleanup;
	} else if (strcmp(arg, "HEARTBEAT") == 0) {
		type = event_heartbeat;
	} else if (isdigit(arg[0])) {
		errno = 0;
		n = strtoul(arg, &p, 10);
		if (*p || errno || n > 127) {
			grecs_error(&val->locus, 0, "invalid exit code");
			return NULL;
		}
		code = n;
		type = event_exit;
	} else {
		grecs_error(&val->locus, 0,
			    "not an exit code or signal number");
		return NULL;
	}

	evt = ecalloc(1, sizeof(*evt));
	evt->code = code;
	evt->type = type;
	return evt;
}

static void
event_list_free(event_t *evt)
{
	while (evt) {
		event_t *next = evt->next;
		free(evt);
		evt = next;
	}
}

static event_t *
parse_event_list(grecs_node_t *node)
{
	event_t *evt_head = NULL, *evt_tail = NULL, *evt;
	struct grecs_list_entry *ep;
	grecs_value_t *val = node->v.value;
	int i;
	
	switch (val->type) {
	case GRECS_TYPE_STRING:
		return parse_event(val);
		
	case GRECS_TYPE_LIST:
		for (ep = val->v.list->head; ep; ep = ep->next)	{
			grecs_value_t *vp = ep->data;
			if (assert_grecs_value_type(&vp->locus, vp,
						    GRECS_TYPE_STRING))
				return NULL;
			evt = parse_event(vp);
			if (!evt) {
				event_list_free(evt_head);
				return NULL;
			}
			if (evt_tail)
				evt_tail->next = evt;
			else
				evt_head = evt;
			evt_tail = evt;
		}
		break;
		
	case GRECS_TYPE_ARRAY:
		for (i = 0; i < val->v.arg.c; i++) {
			if (assert_grecs_value_type(&val->v.arg.v[i]->locus,
						    val->v.arg.v[i],
						    GRECS_TYPE_STRING))
				return NULL;
			evt = parse_event(val->v.arg.v[i]);
			if (!evt) {
				event_list_free(evt_head);
				return NULL;
			}
			if (evt_tail)
				evt_tail->next = evt;
			else
				evt_head = evt;
			evt_tail = evt;
		}
		break;
	}
	return evt_head;
}

static int
cb_onevent(enum grecs_callback_command cmd, grecs_node_t *node,
	   void *varptr, void *cb_data)
{
	listener_t *lp = varptr;
	action_t *act, **pact = cb_data;
	event_t *evt;
	
	switch (cmd) {
	case grecs_callback_section_begin:
		evt = parse_event_list(node);
		if (!evt)
			return 1;
		act = ecalloc(1, sizeof(act[0]));
		act->options = lp->options;
		act->timeout = 5;
		act->evt = evt;
		*pact = act;
		break;

	case grecs_callback_section_end:
		act = *pact;
		act->locus = node->locus;
		act->locus.beg.file = estrdup(act->locus.beg.file);
		act->locus.end.file = estrdup(act->locus.end.file);

		if (lp->act_tail)
			lp->act_tail->next = act;
		else
			lp->act_head = act;
		lp->act_tail = act;
		break;

	case grecs_callback_set_value:
		grecs_error(&node->locus, 0, "invalid use of block statement");
	}
	return 0;
}		

static struct grecs_keyword onevent_kw[] = {
	{ "command", NULL, "run this command",
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(action_t,command) },
	{ "option", NULL, "List of additional options",
	  grecs_type_string, GRECS_LIST, NULL,
	  offsetof(action_t,options),
	  cb_option },
	{ "environ", "<arg: string> <arg: string>...", "Modify environment",
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(action_t,env),
	  cb_environ },
	{ "timeout", "seconds", "command execution timeout",
	  grecs_type_uint, GRECS_DFLT, NULL,
	  offsetof(action_t,timeout) },	  
	{ NULL }
};

static struct grecs_keyword listen_kw[] = {
	{ "interface", NULL, "interface to listen",
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(listener_t,iface), cb_interface },
	{ "match-source", NULL, "match packets coming from these IP addresses",
	  grecs_type_cidr, GRECS_LIST, NULL,
	  offsetof(listener_t,match_source), cb_match_list },
	{ "match-destination", NULL, "match packets to these IP addresses",
	  grecs_type_cidr, GRECS_LIST, NULL,
	  offsetof(listener_t,match_dest), cb_match_list },
	{ "command", NULL, "run this command to bring up the link",
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(listener_t,prog) },
	{ "option", NULL, "List of additional options",
	  grecs_type_string, GRECS_LIST, NULL,
	  offsetof(listener_t,options),
	  cb_option },
	{ "environ", "<arg: string> <arg: string>...", "Modify environment",
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(listener_t,env),
	  cb_environ },
	{ "onevent", "code: string",
	  "Run specified action on the given event",
	  grecs_type_section, GRECS_DFLT, NULL, 0,
	  cb_onevent, NULL, onevent_kw },
	  
	{ NULL }
};

static int
cb_listen(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
	int err = 0;
	listener_t *lp, **plp = cb_data;
	
	switch (cmd) {
	case grecs_callback_section_begin:
		if (assert_grecs_value_type(&node->locus,
					    node->v.value, GRECS_TYPE_STRING))
			return 1;
		lp = ecalloc(1, sizeof(*lp));
		lp->id = estrdup(node->v.value->v.string);
		lp->locus = node->locus;
		*plp = lp;
		break;

	case grecs_callback_section_end:
		lp = *plp;

		if (!lp->iface) {
			grecs_error(&node->locus, 0, "interface missing");
			++err;
		}
		
		if (!lp->match_dest) {
			grecs_error(&node->locus, 0,
				    "match-destination missing");
			++err;
		}
		
		if (!lp->prog) {
			grecs_error(&node->locus, 0, "command missing");
			++err;
		}

		if (!err) {
			listener_t *p;

			listener_fixup(lp);

			p = listener_list_find_id(&clist, lp->id);
			if (p) {
				grecs_error(&node->locus, 0,
					    "duplicate listener");
				grecs_error(&p->locus, 0,
					    "this is the prior one");
				listener_free(lp);
				return 0;
			}

			p = listener_list_find_id(&llist, lp->id);
			if (!p) {
				interface_ref(lp->iface);
				listener_list_add(&clist, lp);
			} else if (listener_cmp(p, lp)) {
				interface_ref(lp->iface);
				listener_list_add(&clist, lp);
				listener_decommission(p);
			} else
				listener_free(lp);
		}
		break;

	case grecs_callback_set_value:
		grecs_error(&node->locus, 0, "invalid use of block statement");
	}
	return 0;
}		
			


static struct grecs_keyword jumper_kw[] = {
	{ "foreground", NULL, "Run in foreground",
	  grecs_type_bool, GRECS_DFLT, &newcfg.foreground },
	{ "pidfile", "file", "Set pid file name",
	  grecs_type_string, GRECS_DFLT, &newcfg.pidfile },
	{ "syslog", NULL, "Configure syslog logging",
	  grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL, syslog_kw },
	{ "debug", "level", "Set debug level",
	  grecs_type_int, GRECS_DFLT, &newcfg.debug_level },
	{ "shutdown-timeout", "seconds", "Set shutdown timeout",
	  grecs_type_uint, GRECS_DFLT, &newcfg.shutdown_timeout },
	{ "heartbeat", "seconds", "Set heartbeat interval",
	  grecs_type_uint, GRECS_DFLT, &newcfg.heartbeat },
	{ "listen", "id: string", "Configure listener",
	  grecs_type_section, GRECS_DFLT, NULL, 0,
	  cb_listen, NULL, listen_kw },
	{ NULL }
};

void
config_help()
{
	static char docstring[] =
		"Configuration file structure for jumper.\n"
		"For more information, see jumper(8), section CONFIGURATION.";
	grecs_print_docstring(docstring, 0, stdout);
	grecs_print_statement_array(jumper_kw, 1, 0, stdout);
}

void
config_init()
{
	grecs_log_to_stderr = 1;
        grecs_include_path_setup(DEFAULT_VERSION_INCLUDE_DIR,
                                 DEFAULT_INCLUDE_DIR, NULL);
        grecs_preprocessor = DEFAULT_PREPROCESSOR;
	grecs_print_diag_fun = jumper_print_grecs_diag;
}

void
config_preparse()
{
	memset(&newcfg, 0, sizeof(newcfg));
	newcfg.facility = LOGFACILITY;
}

int
config_finish(struct grecs_node *tree, int reconfig)
{	
	int rc = grecs_tree_process(tree, jumper_kw) || grecs_error_count;
	grecs_tree_free(tree);
	if (rc == 0) {
		if (newcfg.foreground != config.foreground) {
			if (reconfig)
				grecs_warning(NULL, 0,
				      "ignoring change in foreground setting");
			else
				config.foreground = newcfg.foreground;
		}

		if (!newcfg.pidfile)
			newcfg.pidfile = estrdup(DEFAULT_PIDFILE);

		if (!newcfg.heartbeat)
			newcfg.heartbeat = DEFAULT_HEARTBEAT;
		
		if (reconfig) {
			if (strcmp(newcfg.pidfile, config.pidfile)) {
				unlink(config.pidfile);
				free(config.pidfile);
				config.pidfile = newcfg.pidfile;
				storepid(config.pidfile);
			} else
				free(newcfg.pidfile);
		} else {
			config.pidfile = newcfg.pidfile;
		}

		config.debug_level = newcfg.debug_level;
		config.shutdown_timeout = newcfg.shutdown_timeout;
		config.heartbeat = newcfg.heartbeat;
		config.print_priority = newcfg.print_priority;

		if (!newcfg.tag)
			newcfg.tag = estrdup(program_name);
		
		if (reconfig) {
			if (config.facility != newcfg.facility ||
			    strcmp(config.tag, newcfg.tag)) {
				if (newcfg.tag)
					diag(LOG_INFO,
					     "switching log to facility %d, "
					     "tag %s",
					     newcfg.facility, newcfg.tag);
				else
					diag(LOG_INFO,
					     "switching log to facility %d",
					     newcfg.facility);
				
				closelog();
				config.facility = newcfg.facility;
				if (newcfg.tag) {
					free(config.tag);
					config.tag = newcfg.tag;
				}
			    
				openlog(config.tag, LOG_PID, config.facility);
				diag(LOG_INFO, "log switched");
			} else
				free(newcfg.tag);
		} else {
			config.facility = newcfg.facility;
			config.tag = newcfg.tag;
		}
	} else if (reconfig)
		listeners_undo_decommission();
	return rc;
}
