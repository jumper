/* This file is part of Jumper
   Copyright (C) 1999-2005,2008,2013,2017,2020 Sergey Poznyakoff

   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "jumper.h"

void 
ip_handler(struct ip *ip, unsigned int length)
{
	char ipbuf[2][16];
	char *kve[KVE_MINSIZE+6];
	char *locstr = NULL;
	size_t locsize = 0;
  	listener_t *lp;

	lp = listener_locate(ntohl(ip->ip_src.s_addr),
			     ntohl(ip->ip_dst.s_addr));

	strncpy(ipbuf[0], inet_ntoa(ip->ip_src), sizeof(ipbuf[0]));
	strncpy(ipbuf[1], inet_ntoa(ip->ip_dst), sizeof(ipbuf[1]));

	if (config.debug_level > 2) {
		if (lp) {
			grecs_asprint_locus(&locstr, &locsize, &lp->locus);
			diag(LOG_DEBUG, "%s (%#8x) => %s (%#8x): %s",
			     ipbuf[0], ip->ip_src.s_addr,
			     ipbuf[1], ip->ip_dst.s_addr, locstr);
			free(locstr);
		} else
			diag(LOG_DEBUG, "%s (%#8x) => %s (%#8x): no handler",
			     ipbuf[0], ip->ip_src.s_addr,
			     ipbuf[1], ip->ip_dst.s_addr);
	}

	if (!lp)
		return;

	switch (lp->status) {
	case stat_up:
		return;
	case stat_term:
		listener_print_status(lp);
		onexit_reaction(lp);
		if (lp->status == stat_onexit)
			return;
	case stat_down:
		break;
	case stat_onexit:
		return;
	}

	if (config.debug_level && config.debug_level <= 2) {
		grecs_asprint_locus(&locstr, &locsize, &lp->locus);
		diag(LOG_DEBUG, "%s (%#8x) => %s (%#8x): %s: request %s",
		     ipbuf[0], ip->ip_src.s_addr,
		     ipbuf[1], ip->ip_dst.s_addr,
		     locstr,
		     lp->prog);
		free(locstr);
	}

	listener_kve_init(kve, sizeof(kve)/sizeof(kve[0]),
			  lp,
			  "action", "listener",
			  "src", ipbuf[0],
			  "dst", ipbuf[1],
			  NULL);

	listener_start(lp, kve);
}


