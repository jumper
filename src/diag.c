/* This file is part of Jumper
   Copyright (C) 2013, 2017, 2020 Sergey Poznyakoff
  
   Jumper is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
  
   Jumper is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with Jumper.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "jumper.h"

const char *program_name;
int log_to_stderr = LOG_DEBUG;

void
set_program_name(const char *arg)
{
	char *p = strrchr(arg, '/');
	if (p)
		program_name = p + 1;
	else
		program_name = arg;
}

void
jumper_print_grecs_diag(grecs_locus_t const *locus, int err, int errcode,
			const char *msg)
{
	char *locstr = NULL;
	
	if (locus) {
		size_t size = 0;
		grecs_asprint_locus(&locstr, &size, locus);
	}
  
	if (locstr) {
		if (errcode)
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s: %s",
			     locstr, msg, strerror(errcode));
		else
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s",
			     locstr, msg);
		free(locstr);
	} else {
		if (errcode)
			diag(err ? LOG_ERR : LOG_WARNING, "%s: %s", msg,
			     strerror(errcode));
		else
			diag(err ? LOG_ERR : LOG_WARNING, "%s", msg);
	}
}

/* Diagnostic functions */
static const char *
severity(int prio)
{
	switch (prio) {
	case LOG_EMERG:
		return "EMERG";
	case LOG_ALERT:
		return "ALERT";
	case LOG_CRIT:
		return "CRIT";
	case LOG_ERR:
		return "ERROR";
	case LOG_WARNING:
		return "WARNING";
	case LOG_NOTICE:
		return "NOTICE";
	case LOG_INFO:
		return "INFO";
	case LOG_DEBUG:
		return "DEBUG";
	}
	return NULL;
}

void
vdiag(int prio, const char *fmt, va_list ap)
{
	const char *s;
	va_list tmp;
	
	if (log_to_stderr >= prio) {
		fprintf(stderr, "%s: ", program_name);
		s = severity(prio);
		if (s)
			fprintf(stderr, "[%s] ", s);
		va_copy(tmp, ap);
		vfprintf(stderr, fmt, tmp);
		fputc('\n', stderr);
		va_end(tmp);
	}

	if (config.facility > 0) {
		if (config.print_priority && (s = severity(prio)) != NULL) {
			static char *fmtbuf;
			static size_t fmtsize;
			size_t len = strlen(fmt) + strlen(s) + 4;
			char *p;
			
			if (len > fmtsize) {
				fmtbuf = erealloc(fmtbuf, len);
				fmtsize = len;
			}

			p = fmtbuf;
			*p++ = '[';
			while (*s)
				*p++ = *s++;
			*p++ = ']';
			*p++ = ' ';
			while ((*p++ = *fmt++));
			vsyslog(prio, fmtbuf, ap);
		} else			
			vsyslog(prio, fmt, ap);
	}
}

void
diag(int prio, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vdiag(prio, fmt, ap);
	va_end(ap);
}
	
void
debugprt(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vdiag(LOG_DEBUG, fmt, ap);
	va_end(ap);
}
