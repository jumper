.\" This file is part of jumper -*- nroff -*-
.\" Copyright (C) 2017, 2020 Sergey Poznyakoff
.\"
.\" Jumper is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Jumper is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with jumper.  If not, see <http://www.gnu.org/licenses/>.
.TH IFALIVE 1 "April 7, 2017" "JUMPER" "Jumper User Reference"
.SH NAME
ifalive \- check network interface activity
.SH SYNOPSIS
.nh
.na
\fBifalive\fR \fB\-i\fR [\fB\-dnp\fR] \fIIFACE\fR [\fIPID\fR]
.PP
\fBifalive\fR [\fB\-c\fR] [\fB\-dn\fR] [\fB\-s\fR \fISIG\fR] \fIPID\fR
[\fIEXPR\fR...]
.PP
\fBifalive \-r\fR [\fB\-dnp\fR] [\fIPID\fR]
.PP
.B ifalive \-h
.ad
.hy
.SH DESCRIPTION
Given the network interface name and the PID of the program that
controls it, \fBifalive\fR checks if the interface is "\fIalive\fR",
and if not, terminates the program.
.PP
It is designed as an ancillary utility for
.BR jumper (8),
to help automatically shut down inactive interfaces.
.PP
The program runs in three different operation modes, selected by the
options
.BR \-i,
.BR \-c,
and
.BR \-r .
.SS Initialization
The \fB\-i\fR option requests \fIinitialization mode\fR.  In this
mode, \fIifalive\fR remembers information about the network interface,
and the PID of the corresponding process that controls it.
.PP
If the \fIPID\fR argument is not given, the program assumes the PID of its
parent process.  The \fB\-p\fR option, if used, instructs the program
to use PID of the parent process of the originally obtained PID.  For
example,
.RS
.nf
.B ifalive -i -p tun0 1822
.fi
.RE
means that interface \fBtun0\fR is controlled by the parent of PID
1822, whereas
.RS
.nf
.B ifalive -i -p tun0
.fi
.RE
means that it is controlled by the grand-parent of \fBifalive\fR
itself.  This option can be used multiple times to move up the process
ancestor tree.  For example,
.RS
.nf
.B ifalive -i -ppp tun0 1822
.fi
.RE
means use grand-grand-parent of PID 1822.
.PP
The information is stored in a disk file
\fB/var/run/ifalive/\fIPID\fR.  This file will then be used to store
intermediate information about the state of the interface.
.PP
The initialization is normally performed right after the corresponding
interface is started.  For example, supposing the interface is
controlled by
.BR vpnc (8),
the following line should appear in the
.B  /etc/vpnc/vpnc-script-post-connect-action
file:
.sp
.RS
.nf
.B /usr/libexec/ifalive -i -pp $TUNDEV
.fi
.RE
.SS Periodic checking
This is the main (and the default) operation mode.  It is requested by
the \fB\-c\fR option, or by the absence of the mode specifying
option.  The \fIPID\fR argument is required.  The tool will first look
up the interface name corresponding to that \fIPID\fR, and will
evaluate the expression \fIEXPR\fR to check if the interface is
active.  In the absence of \fIEXPR\fR, the default expression is
evaluated.  See the section
.BR EXPRESSION ,
for the description.
.PP
If the expression evaluates to \fBtrue\fR, the utility will update the
interface statistics in its spool file, and will exit.  Otherwise, it
will terminate the controlling program by sending the \fBSIGTERM\fR
signal to \fIPID\fR.
.PP
The check mode is normally run periodically in a \fBHEARTBEAT\fR
event:
.sp
.RS
.nf
onevent HEARTBEAT {
    command "/usr/libexec/ifalive -c $pid";
}
.fi
.RE
.PP
.SS Cleanup
The cleanup mode is requested by the \fB\-r\fR command line option.
In this mode, \fBifalive\fR removes the state file for the given
\fIPID\fR.  The actual PID is computed as described in subsection
.BR Initialization ,
above.
.PP
This mode should be run when the interface goes down.  For example, if
the interface is controlled by \fBvpnc\fR, the following line should
appear in the file
.BR /etc/vpnc/vpnc-script-post-disconnect-action :
.sp
.RS
.nf
.B /usr/libexec/ifalive -r -pp 
.fi
.RE
.SH EXPRESSION
The expression operates on two variables: the previous interface state
\fBa\fR, and its current state, \fBb\fR.  Both variables have a number
of numeric attributes, which are accessed as \fB\fIX\fB.\fIattr\fR,
where \fIX\fR stands for the variable and \fIattr\fR for the attribute
name.  The following attributes are defined:
.TP
.B rx_bytes
Number of bytes received.
.TP
.B rx_packets
Number of network packets received.
.TP
.B rx_errors
Number of errors while receiving.
.TP
.B rx_dropped
Number of dropped incoming (received) packets.
.TP
.B rx_fifo_errors
Number of FIFO errors on incoming packets.
.TP
.B rx_frame_errors
Number of received frame errors.
.TP
.B rx_compressed
Number of received compressed format packets.
.TP
.B rx_multicast
Number of received multicast packets.
.TP
.B tx_bytes
Number of bytes transmitted.
.TP
.B tx_packets
Number of network packets transmitted.
.TP
.B tx_errors
Number of transmit errors.
.TP
.B tx_dropped
Number of dropped transmit packets.
.TP
.B tx_fifo_errors
Number of FIFO errors on transmit.
.TP
.B collisions
Number of collisions.
.TP
.B tx_carrier_errors
Number of carrier errors.
.TP
.B tx_compressed
Number of transmitted compressed format packets.
.PP
The usual arithmetical operations (\fB+\fR, \fB\-\fR,
\fB*\fR, \fB/\fR) are allowed over these values and integer numeric
constants.  The results can be compared using the operators:
\fB==\fR (or \fB==\fR), \fB!=\fR, \fB<\fR, \fB<=\fR, \fB>\fR,
\fB>=\fR.  The comparisons can be combined using boolean operations
\fBor\fR and \fBand\fR.  Logical values are negated using \fB!\fR.
The following table lists the operators in order of their precedence.
Operators down the table are executed prior to those located above.
Operators on the same line are executed left to right.
.PP
.sp
.nf
.ta 8n
	\fBor\fR
	\fBand\fR
	\fB=\fR, \fB==\fR, \fB!=\fR
	\fB<\fR, \fB<=\fR, \fB>\fR, \fB>=\fR
	\fB+\fR, \fB\-\fR
	\fB*\fR, \fB/\fR
	unary \fB!\fR, unary \fB\-\fR
.fi
.PP
Braces can be used to control the precedence.
.PP
The default expression is:
.PP
.RS
.nf
.B a.rx_bytes != b.rx_bytes and a.tx_bytes != b.tx_bytes
.fi
.RE
.PP
It means that interface is considered alive if some bytes were
transmitted or received over it since the last check.
.SH OPTIONS
.SS Actions
.TP
.B \-i
Initializes the state file.
.TP
.B \-c
Checks the state of the link (the default).
.TP
.B \-r
Removes the state file for \fIPID\fR
.TP
.B \-h
Displays a help summary.
.SS Modifiers
.TP
.B \-d
Enable debug output.
.TP
.B \-n
Dry-run mode: print everything, do nothing.
.TP
.B \-p
Use parent of \fIPID\fR (grand-parent for \fB\-pp\fR, etc.)
.TP
\fB\-s\fR \fISIG\fR
Terminate idle process with signal \fISIG\fR, instead of the default
\fBSIGTERM\fR.
.SH FILES
.TP
.B /proc/net/dev
Network interface statistics.
.TP
.B /var/run/ifalive
Directory where interface state files are stored.  Files are named by
the corresponding PID.
.SH BUGS
The program is Linux-specific.
.SH "SEE ALSO"
.BR jumper (8),
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <bug\-jumper@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2017, 2020 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

