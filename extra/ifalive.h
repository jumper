enum {
	stat_rx_bytes,
	stat_rx_packets,
	stat_rx_errors,
	stat_rx_dropped,
	stat_rx_fifo_errors,
	stat_rx_frame_errors,
	stat_rx_compressed,
	stat_rx_multicast,
	stat_tx_bytes,
	stat_tx_packets,
	stat_tx_errors,
	stat_tx_dropped,
	stat_tx_fifo_errors,
	stat_collisions,
	stat_tx_carrier_errors,
	stat_tx_compressed,

	MAX_STATS
};

enum {
	VAR_PREV,
	VAR_THIS
};

typedef uintmax_t IFSTAT[MAX_STATS];
#define IFSTAT_SIZE (sizeof(uintmax_t) * MAX_STATS)
int expr_eval(int argc, char **argv, IFSTAT a, IFSTAT b);

void error(int ec, char const *fmt, ...);
void lex_init(int argc, char **argv);


