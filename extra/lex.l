%{
#include <config.h>
#include <stdlib.h>
#include <stdint.h>
#include "expr.h"
#include "ifalive.h"
	
#define RET_STAT(s) { yylval.field = s; return FIELD; }

#undef YY_INPUT
#define YY_INPUT(buf,result,max_size) result = fillbuf(buf, max_size)

static int lex_argc;
static char **lex_argv;
static int lex_argi;
static char *lex_curp;

static size_t
fillbuf(char *buf, size_t max_size)
{
	size_t consumed = 0;
	
	while (consumed < max_size) {
		size_t n;
		
		if (!*lex_curp) {
			if (lex_argi == lex_argc)
				break;
			if (lex_argi)
				buf[consumed++] = ' ';
			lex_curp = lex_argv[lex_argi++];
		}
		n = strlen(lex_curp);
		if (n > max_size - consumed)
			n = max_size - consumed;
		memcpy(buf + consumed, lex_curp, n);
		lex_curp += n;
		consumed += n;
	}

	return consumed;
}

void
lex_init(int argc, char **argv)
{
	lex_argc = argc;
	lex_argv = argv;
	lex_argi = 1;
	lex_curp = argv[0];
}

void
yyerror(char const *s)
{
	if (lex_curp)
		error(0, "%s (near %s)", s, lex_curp);
	else
		error(0, "%s (at the end of input)", s);
}

static int
yywrap(void)
{
	return 1;
}
%}
%%
"or"                            return OR;
"and"                           return AND;
"="                             return EQ;
"=="                            return EQ;
"!="                            return NE;
"<"                             return LT;
"<="                            return LE;
">"                             return GT;
">="                            return GE;
"!"                             return NOT;
rx_bytes                 	RET_STAT(stat_rx_bytes);          
rx_packets               	RET_STAT(stat_rx_packets);        
rx_errors                	RET_STAT(stat_rx_errors);         
rx_dropped               	RET_STAT(stat_rx_dropped);        
rx_fifo_errors           	RET_STAT(stat_rx_fifo_errors);    
rx_frame_errors          	RET_STAT(stat_rx_frame_errors);   
rx_compressed            	RET_STAT(stat_rx_compressed);     
rx_multicast             	RET_STAT(stat_rx_multicast);      
tx_bytes                 	RET_STAT(stat_tx_bytes);          
tx_packets               	RET_STAT(stat_tx_packets);        
tx_errors                	RET_STAT(stat_tx_errors);         
tx_dropped               	RET_STAT(stat_tx_dropped);        
tx_fifo_errors           	RET_STAT(stat_tx_fifo_errors);    
collisions               	RET_STAT(stat_collisions);        
tx_carrier_errors        	RET_STAT(stat_tx_carrier_errors); 
tx_compressed            	RET_STAT(stat_tx_compressed);
a                               { yylval.var = VAR_PREV; return VAR; }
b                               { yylval.var = VAR_THIS; return VAR; }
[0-9][0-9]*                     { yylval.value = strtoumax(yytext, NULL, 10);
	                        return VALUE; }
[ \t\n]                         ;
.                               return yytext[0];
