#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>
#include <assert.h>
#include "ifalive.h"

#ifndef PATH_PROCNET_DEV
# define PATH_PROCNET_DEV "/proc/net/dev"
#endif

char *progname;
char *spooldir = "/var/run/ifalive";
int debug;
int dry_run;
int sig = SIGTERM;
int parent;

#define EX_OK 0
#define EX_FAIL 1

void
error(int ec, char const *fmt, ...)
{
	va_list ap;
	fprintf (stderr, "%s: ", progname);
	va_start (ap, fmt);
	vfprintf (stderr, fmt, ap);
	va_end (ap);
	if (ec) 
		fprintf(stderr, ": %s", strerror(ec));
	fputc('\n', stderr);
}

void
usage(int code)
{
	FILE *fp = code ? stderr : stdout;
	fprintf(fp,
		"Usage: ifalive -i [-dnp] IFACE [PID]\n"
		"       ifalive [-c] [-dn] [-s SIG] PID [EXPR...]\n"
		"       ifalive -r [-dnp] [PID]\n"
		"       ifalive -h\n"
		"\n"
		"Actions:\n"
		"\n"
		"   -i          initializes the state file\n"
		"   -c          checks the state of the link (the default)\n"
		"   -r          removes the state file for PID\n"
		"   -h          displays this help summary\n"
		"\n"
		"Options:\n"
		"\n"
		"   -d          debug information\n"
		"   -n          dry-run: print everything, do nothing\n"
		"   -p          use parent of PID (grand-parent for -pp, etc.)\n"
		"   -s SIG      terminate idle process with signal SIG, instead\n"
		"               of SIGTERM\n"
		);
	exit(code);
}

static size_t
trimnl(char *s)
{
	size_t n = strlen(s);
	if (n > 0 && s[n-1] == '\n')
		s[--n] = 0;
	return n;
}

static int
getnum(uintmax_t *ret, char **pptr)
{
	char *p = *pptr;
	uintmax_t acc = 0;
	static char digits[] = "0123456789";

	while (*p && (*p == ' ' || *p == '\t'))
		p++;

	if (!*p || !strchr(digits, *p)) {
		*pptr = p;
		return -1;
	}
	
	while (*p) {
		char *q = strchr(digits, *p);
		if (!q)
			break;
		p++;
		acc = acc * 10 + q - digits;
	}
	
	*pptr = p;
	*ret = acc;
	return 0;
}

static int
procnet_parse_line(char *p, char **iface, uintmax_t statbuf[MAX_STATS])
{
	int i;
	char *ifp;
	
	while (*p && (*p == ' ' || *p == '\t'))
		p++;
	ifp = p;
	while (*p && *p != ':')
		p++;
	if (!*p)
		return -1;
	*p++ = 0;
	*iface = ifp;
	for (i = 0; i < MAX_STATS; i++) {
		if (getnum(&statbuf[i], &p))
			return -1;
	}
	return 0;
}

void
procnet_write_line(FILE *fp, char const *iface, uintmax_t statbuf[MAX_STATS])
{
	int i;
	
	fprintf(fp, "%s:", iface);
	for (i = 0; i < MAX_STATS; i++) 
		fprintf(fp, " %jd", statbuf[i]);
	fputc('\n', fp);
}

int
procnet_read(char const *iface, uintmax_t statbuf[MAX_STATS])
{
	FILE *fp;
	char buf[1024];
	int rc;
	
	fp = fopen(PATH_PROCNET_DEV, "r");
	if (!fp) {
		error(errno, "can't open \"%s\"", PATH_PROCNET_DEV);
		return EX_FAIL;
	}

	if (!fgets(buf, sizeof buf, fp)
	    || !fgets(buf, sizeof buf, fp)) {
		error(errno, "%s: malformed file");
		return EX_FAIL;
	}
	
	rc = EX_FAIL;
	while (fgets(buf, sizeof buf, fp)) {
		char *ifp;

		trimnl(buf);
		if (procnet_parse_line(buf, &ifp, statbuf)) {
			error(errno, "%s: malformed file", PATH_PROCNET_DEV);
			break;
		}
		
		if (strcmp(iface, ifp) == 0) {
			rc = EX_OK;
			break;
		}
	}
	fclose(fp);
	return rc;
}

static pid_t
getparentpid(pid_t pid)
{
	char fname[512];
	FILE *fp;
	unsigned long ppid;
	int n, ec;
	
	snprintf(fname, sizeof fname, "/proc/%lu/stat", (unsigned long) pid);
	fp = fopen(fname, "r");
	if (!fp) {
		error(errno, "can't open \"%s\" for reading", fname);
		return (pid_t) -1;
	}

	n = fscanf(fp, "%*u %*s %*s %lu", &ppid);
	ec = errno;
	fclose(fp);
	if (n != 1) {
		error(ec, "error reading from \"%s\"", fname);
		return (pid_t) -1;
	}

	return ppid;
}

char *
argpid(char *arg)
{
	char pbuf[512];
	char *pstr;
	pid_t pid;
	
	if (arg) {
		char *p;
		pid = strtoul(arg, &p, 10);
		if (*p) {
			error(0, "invalid PID");
			return NULL;
		}
	} else
		pid = getppid();

	for (; parent; parent--) {
		pid = getparentpid(pid);
		if (pid == 1 || pid == (pid_t)-1)
			return NULL;
	}
	snprintf(pbuf, sizeof(pbuf), "%lu", (unsigned long)pid);
	pstr = strdup(pbuf);
	if (!pstr) 
		error(errno, "strdup");
	return pstr;
}

int
ifalive_init(int argc, char **argv)
{
	FILE *fp;
	char *pstr;
	
	if (argc < 1 || argc > 2)
		usage(EX_FAIL);
	
	pstr = argpid(argv[1]);
	if (!pstr)
		return EX_FAIL;

	if (debug)
		printf("writing \"%s/%s\"; %s\n", spooldir, pstr, argv[0]);
	if (dry_run)
		return EX_OK;
	
	fp = fopen(pstr, "w");
	if (!fp) {
		error(errno, "can't open \"%s/%s\" for writing", spooldir,
		      pstr);
		return EX_FAIL;
	}
	fprintf(fp, "%s\n", argv[0]);
	fclose(fp);
	return EX_OK;
}

int
ifalive_remove(int argc, char **argv)
{
	char *pstr;
	
	if (argc > 1)
		usage(EX_FAIL);
	
	pstr = argpid(argv[0]);
	if (!pstr)
		return EX_FAIL;

	if (debug)
		printf("removing %s/%s\n", spooldir, pstr);
			
	if (!dry_run && unlink(pstr)) {
		error(errno, "%s/%s: can't unlink", spooldir, pstr);
		return EX_FAIL;
	}
	return EX_OK;
}

static int def_argc = 1;
static char *def_argv[] = {
	"a.rx_bytes != b.rx_bytes and a.tx_bytes != b.tx_bytes",
	NULL
};

int
ifalive_check(int argc, char **argv)
{
	FILE *fp;
	char buf[512];
	char *iface;
	char *pidstr;
	uintmax_t statbuf[MAX_STATS], snapshot[MAX_STATS];
	
	if (argc == 0)
		usage(EX_FAIL);
	
	pidstr = argv[0];
	argc--;
	argv++;
	
	if (argc == 0) {
		argc = def_argc;
		argv = def_argv;
	}
	
	fp = fopen(pidstr, "r+");
	if (!fp) {
		error(errno, "can't open \"%s/%s\"", spooldir, pidstr);
		return EX_FAIL;
	}
	
	if (!fgets(buf, sizeof buf, fp)) {
		error(errno, "fgets");
		return EX_FAIL;
	}
	
	trimnl(buf);
	if (strchr(buf, ':') == 0) {
		iface = buf;
		memset(snapshot, 0, sizeof snapshot);
	} else if (procnet_parse_line(buf, &iface, snapshot)) {
		error(0, "%s/%s: malformed snapshot", spooldir, pidstr);
		return EX_FAIL;
	}
	
	if (debug)
		printf("interface: %s\n", iface);
	
	if (procnet_read(iface, statbuf))
		return EX_FAIL;

	if (expr_eval(argc, argv, snapshot, statbuf) == 0) {
		if (debug)
			printf("%s is IDLE; terminate %s\n", iface, pidstr);
		if (!dry_run) {
			char *endp;
			pid_t pid = strtoul(pidstr, &endp, 10);
			assert(pid >= 2);
			kill(pid, sig);
		}
	}
	rewind(fp);
	procnet_write_line(fp, iface, statbuf);
	fclose(fp);
	return EX_OK;
}

int
main(int argc, char **argv)
{
	int c;
	int (*action) (int argc, char **argv) = ifalive_check;
	
	progname = argv[0];
	
	while ((c = getopt(argc, argv, "cdhinprs:")) != EOF) {
		switch (c) {
		case 'c':
			action = ifalive_check;
			break;
		case 'd':
			debug++;
			break;
		case 'h':
			usage(EX_OK);
		case 'i':
			action = ifalive_init;
			break;
		case 'n':
			dry_run = 1;
			debug++;
			break;
		case 'p':
			parent++;
			break;
		case 'r':
			action = ifalive_remove;
			break;
		case 's':
			sig = strtoul(optarg, NULL, 10);
			break;
		default:
			exit(EX_FAIL);
		}
	}
	
	if (argc == 1) 
		usage(EX_FAIL);
	
	if (chdir(spooldir)) {
		error(errno, "can't change to %s", spooldir);
		return EX_FAIL;
	}
	
	return action(argc - optind, argv + optind);
}
