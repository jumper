%{
#include <config.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include "expr.h"
#include "ifalive.h"

enum expr_datatype {
	dt_int,
	dt_bool,
	dt_double,

	DT_MAX
};

struct expr_var {
	int var;
	int field;
};

enum expr_node_type {
	node_value,
	node_var,
	node_unary,
	node_binary
};

enum expr_binop {
	binop_add,
	binop_sub,
	binop_mul,
	binop_div,
	binop_eq,
	binop_ne,
	binop_gt,
	binop_ge,
	binop_lt,
	binop_le,
	binop_and,
	binop_or
};

enum expr_unop {
	unop_not,
	unop_minus,
	unop_typecast
};

struct expr_node {
	enum expr_node_type type;
	enum expr_datatype datatype;
	union {
		uintmax_t v_int;
		int v_bool;
		double v_double;
		struct expr_var var;
		struct {
			enum expr_binop opcode;
			struct expr_node *operand[2];
		} binary;
		struct {
			enum expr_unop opcode;
			struct expr_node *operand;
		} unary;
	} v;
};

static struct expr_node *
expr_node_alloc(void)
{
	struct expr_node *p = malloc(sizeof(*p));
	assert(p != NULL);
	return p;
}

enum expr_datatype expr_promoted_type[DT_MAX][DT_MAX] = {
	[dt_int] = {
		[dt_int] = dt_int,
		[dt_double] = dt_double,
		[dt_bool] = dt_int
	},
	[dt_double] = {
		[dt_int] = dt_double,
		[dt_double] = dt_double,
		[dt_bool] = dt_double
	},
	[dt_bool] = {
		[dt_int] = dt_int,
		[dt_double] = dt_double,
		[dt_bool] = dt_bool
	},	
};

struct expr_node *
expr_cast(enum expr_datatype t, struct expr_node *a)
{
	if (t != a->datatype) {
		struct expr_node *res = expr_node_alloc();
		res->type = node_unary;
		res->datatype = t;
		res->v.unary.opcode = unop_typecast;
		res->v.unary.operand = a;
		a = res;
	}
	return a;
}
	
static struct expr_node *
expr_binop(struct expr_node *a, struct expr_node *b,
	   enum expr_binop opcode, enum expr_datatype datatype)
{
	struct expr_node *res;
	
	res = expr_node_alloc();
	res->type = node_binary;
	res->datatype = datatype;
	res->v.binary.opcode = opcode;
	res->v.binary.operand[0] = expr_cast(res->datatype, a);
	res->v.binary.operand[1] = expr_cast(res->datatype, b);
	return res;
}

static struct expr_node *
expr_binop_comp(struct expr_node *a, struct expr_node *b,
		enum expr_binop opcode)
{
	struct expr_node *res;
	enum expr_datatype t = expr_promoted_type[a->datatype][b->datatype];
	
	res = expr_node_alloc();
	res->type = node_binary;
	res->datatype = dt_bool;
	res->v.binary.opcode = opcode;
	res->v.binary.operand[0] = expr_cast(t, a);
	res->v.binary.operand[1] = expr_cast(t, b);
	return res;
}

void yyerror(char const *s);
int yylex (void);

static struct expr_node *parse_tree;
%}

%token <var> VAR
%token <field> FIELD	 
%token <value> VALUE
	 
%left OR
%left AND
%left EQ NE
%left LT LE GT GE
%left '+' '-'
%left '*' '/'
%left NOT UMINUS

%type <node> term expr
	 
%union {
	int var;
	int field;
	uintmax_t value;
	struct expr_node *node;
}
	 
%%
input:  expr
        {
		parse_tree = expr_cast(dt_bool, $1);
	}
     ;

expr :   term
     |   NOT expr
         {
		 $$ = expr_node_alloc();
		 $$->type = node_unary;
		 $$->datatype = dt_bool;
		 $$->v.unary.opcode = unop_not;
		 $$->v.unary.operand = expr_cast(dt_bool, $2);
	 }
     |   '-' expr %prec UMINUS
         {
		 $$ = expr_node_alloc();
		 $$->type = node_unary;
		 $$->datatype = dt_double;
		 $$->v.unary.opcode = unop_minus;
		 $$->v.unary.operand = expr_cast(dt_double, $2);
	 }
      |   expr AND expr
	  {
		  $$ = expr_binop($1, $3, binop_and, dt_bool);
	  }
      |   expr OR expr
	  {
		  $$ = expr_binop($1, $3, binop_or, dt_bool);
	  }
      |   expr EQ expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_eq);
	  }
      |   expr NE expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_ne);
	  }
      |   expr LT expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_lt);
	  }
      |   expr LE expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_le);
	  }
      |   expr GT expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_gt);
	  }
      |   expr GE expr
	  {
		  $$ = expr_binop_comp($1, $3, binop_ge);
	  }
      |   expr '+' expr
	  {
		  $$ = expr_binop($1, $3, dt_double, binop_add);
	  }
      |   expr '-' expr
	  {
		  $$ = expr_binop($1, $3, dt_double, binop_sub);
	 }
     |   expr '*' expr
         {
		 $$ = expr_binop($1, $3, dt_double, binop_mul);
	 }
     |   expr '/' expr
         {
		 $$ = expr_binop($1, $3, dt_double, binop_div);
	 }
     |   '(' expr ')'
         {
		 $$ = $2;
	 }
     ;

term :   VAR '.' FIELD
         {
		 $$ = expr_node_alloc();
		 $$->type = node_var;
		 $$->datatype = dt_int;
		 $$->v.var.var = $1;
		 $$->v.var.field = $3;
	 }
     |   VALUE
         {
		 $$ = expr_node_alloc();
		 $$->type = node_value;
		 $$->datatype = dt_int;
		 $$->v.v_int = $1;
	 }
     ;

%%

struct eval_env
{
	IFSTAT stat[2];
};

union eval_res {
	uintmax_t v_int;
	int v_bool;
	double v_double;
};

#define EVAL_COMP(op,node,a,b,res)					\
	switch ((node)->v.binary.operand[0]->datatype) {	        \
        case dt_int:                                                    \
		res->v_bool = a.v_int op b.v_int;                       \
		break;                                                  \
	case dt_bool:                                                   \
		res->v_bool = a.v_bool op b.v_bool;                     \
		break;                                                  \
	case dt_double:                                                 \
		res->v_bool = a.v_double op b.v_double;                 \
	}


static void
node_eval(struct expr_node *node, struct eval_env *env, union eval_res *res)
{
	union eval_res a, b;
	
	switch (node->type) {
	case node_value:
		assert(node->datatype == dt_int);
		res->v_int = node->v.v_int;
		break;
		
	case node_var:
		res->v_int = env->stat[node->v.var.var][node->v.var.field];
		break;
		
	case node_binary:
		node_eval(node->v.binary.operand[0], env, &a);
		node_eval(node->v.binary.operand[1], env, &b);

		switch (node->v.binary.opcode) {
		case binop_add:
			assert(node->datatype == dt_double);
			res->v_double = a.v_double + b.v_double;
			break;

		case binop_sub:
			assert(node->datatype == dt_double);
			res->v_double = a.v_double - b.v_double;
			break;
			
		case binop_mul:
			assert(node->datatype == dt_double);
			res->v_double = a.v_double * b.v_double;
			break;
			
		case binop_div:
			assert(node->datatype == dt_double);
			res->v_double = a.v_double / b.v_double;
			break;
			
		case binop_eq:
			EVAL_COMP(==, node, a, b, res);
			break;
			
		case binop_ne:
			EVAL_COMP(!=, node, a, b, res);
			break;
			
		case binop_gt:
			EVAL_COMP(>, node, a, b, res);
			break;
			
		case binop_ge:
			EVAL_COMP(>=, node, a, b, res);
			break;
			
		case binop_lt:
			EVAL_COMP(<, node, a, b, res);
			break;
			
		case binop_le:
			EVAL_COMP(<=, node, a, b, res);
			break;

		case binop_and:
			assert(node->datatype == dt_bool);
			res->v_bool = a.v_bool && b.v_bool;
			break;

		case binop_or:
			assert(node->datatype == dt_bool);
			res->v_bool = a.v_bool || b.v_bool;
			break;
		}
		break;
		
	case node_unary:
		node_eval(node->v.unary.operand, env, &a);
		switch (node->v.unary.opcode)
		{
		case unop_not:
			assert(node->datatype == dt_bool);
			res->v_bool = !a.v_bool;
			break;
			
		case unop_minus:
			assert(node->datatype == dt_double);
			res->v_double = - a.v_double;
			break;

		case unop_typecast:
			switch (node->datatype) {
			case dt_int:
				res->v_int = a.v_bool;
				break;
			case dt_double:
				switch (node->v.unary.operand->datatype) {
				case dt_int:
					res->v_double = a.v_int;
					break;
				case dt_bool:
					res->v_double = a.v_bool;
					break;
				default:
					abort();
				}
			default:
				abort();
			}
		}
		break;
	}
}		

int
expr_eval(int argc, char **argv, IFSTAT a, IFSTAT b)
{
	struct eval_env env;
	union eval_res res;
	
	lex_init(argc, argv);
	if (yyparse())
		exit(1);
	memcpy(env.stat[0], a, IFSTAT_SIZE);
	memcpy(env.stat[1], b, IFSTAT_SIZE);
	node_eval(parse_tree, &env, &res);
	return res.v_bool;
}
